**Calculate damage inflict by wizard's spells**


.. image:: http://img.shields.io/:license-mit-blue.svg
  :alt: MIT License
  :target: http://doge.mit-license.org

.. image:: https://readthedocs.org/projects/sultan/badge/?version=latest
  :alt: Documentation Status
  :target: https://bitbucket.org/grzelakmichal/damage/src/e4332ec9eefc9077d8a8be55a079516cc9bf0822/docs/task_2.md?at=master&fileviewer=file-view-default

Supports Python 3.0+

-------

----------------------
What is Damage Module?
----------------------

Damage is module which parsing string `spells` and calculate damage inflict by spell
The simplest way to use Damage is to just call it:

.. code:: python

  from damage_module.core import damage
  
  inflicted_damage = damage("feeai")
  print(inflicted_damage)