# coding=utf-8
"""
INIT
"""
from .core import damage
from .helpers import (
    first_occurrence_from_end,
    is_occur_once_and_only_once,
    generate_all_subspells,
    max_damage)
from .constans import (
    SPELLS,
    BEGINNING_SUBSPELL,
    ENDING_SUBSPELL,
    max_spell,
    beginning_spell_len,
    beginning_spell_damage,
    ending_spell_damage
)
