# coding=utf-8
"""
Data about:
    spell list
    beginning spell
    ending spell
"""

SPELLS = {'fe': 1,
          'je': 2,
          'jee': 3,
          'ain': 3,
          'dai': 5,
          'ne': 2,
          'ai': 2}

BEGINNING_SUBSPELL = "fe"
ENDING_SUBSPELL = "ai"


def beginning_spell_damage():
    """
    Check beginning spell

    :return: beginning spell damage
    """
    try:
        return SPELLS[BEGINNING_SUBSPELL]
    except KeyError:
        print("Incorrect beginning subspell")


def ending_spell_damage():
    """
    Check ending spell

    :return: ending spell damage
    """
    try:
        return SPELLS[ENDING_SUBSPELL]
    except KeyError:
        print("Incorrect ending subspell")


def beginning_spell_len():
    """
    Calculate beginning spell len

    :return: beginning spell len
    """
    return len(BEGINNING_SUBSPELL)


max_spell = len((max(SPELLS.keys(), key=len)))
