# coding=utf-8

"""
CORE
"""

from . import constans
from . import helpers


def damage(spell):
    """
    Calculate damage inflict by spell

    :param spell: string spell
    :return: int damage
    """
    inflicted_damage = 0

    if helpers.is_occur_once_and_only_once(spell, constans.BEGINNING_SUBSPELL):
        beginning_subspell_index = spell.index(constans.BEGINNING_SUBSPELL)
        spell = spell[beginning_subspell_index:]
        ending_subspell_index = helpers.first_occurrence_from_end(spell, constans.ENDING_SUBSPELL)

        if ending_subspell_index >= 0:
            beginning_subspell_length = constans.beginning_spell_len()
            spell = spell[beginning_subspell_length:ending_subspell_index]
            subspells_list = helpers.generate_all_subspells(spell)

            m = 0
            inflicted_damage += (constans.beginning_spell_damage() + constans.ending_spell_damage())
            spell_length = len(spell)
            while m < len(subspells_list):

                conflict_subspells_list = []
                while m < len(subspells_list) - 1 and subspells_list[m][2] <= subspells_list[m + 1][2] <= \
                        subspells_list[m][3]:

                    conflict_subspells_list.append(subspells_list[m])
                    m += 1

                conflict_subspells_list.append(subspells_list[m])

                if len(conflict_subspells_list) > 1:
                    max_dmg = helpers.max_damage(conflict_subspells_list)
                    inflicted_damage += max_dmg[0]
                    spell_length -= max_dmg[1]
                else:
                    inflicted_damage += subspells_list[m][1]
                    spell_length -= subspells_list[m][4]
                m += 1

            inflicted_damage -= spell_length

    if inflicted_damage < 0:
        return 0
    return inflicted_damage
