# coding=utf-8
"""
HELPERS
"""
import copy
from . import constans


def first_occurrence_from_end(string, substring):
    """
    Return index of first occurrence substring from ending

    :param string: string
    :param substring: substring
    :return: index if substring in string else -1
    """
    string_length = len(string)
    substring_length = len(substring)
    for i in range(string_length):
        if string[string_length - substring_length - i:string_length - i] == substring:
            return string_length - i - substring_length
    return -1


def is_occur_once_and_only_once(string, substring):
    """
    Check if substring is in string occur once and only once

    :param string: string string
    :param substring: string substring
    :return: boolean True if once False otherwise
    """
    string_length = len(string)
    substring_length = len(substring)
    counter = 0
    for i in range(string_length):
        if string[i:i + substring_length] == substring:
            counter += 1

    if counter == 1:
        return True
    return False


def generate_all_subspells(spell):
    """
    Generate list o all possible subspells

    :param spell: string spell
    :return: list of tuples in format: (subspell, damage, begin_index, end_index, subspell len)
    """
    damage_index = []
    j = 0

    cut_spell_length = len(spell)
    while j < cut_spell_length:
        k = 0
        while k < constans.max_spell and j+k < cut_spell_length:
            try:
                subspell = spell[j:j + k + 1]
                damage_index.append((subspell, constans.SPELLS[subspell], j, j + k, len(subspell)))
            except KeyError:
                pass
            k += 1
        j += 1

    return damage_index


def max_damage(conflict_damage):
    """

    :param conflict_damage:
    :return:
    """
    if len(conflict_damage) >= 2:
        damage_list = []

        n = 0
        length = conflict_damage[-1][3] - conflict_damage[0][2] + 1

        def decisions(_conflict_damage, _n, _length):
            """

            :param _conflict_damage:
            :param _n:
            :param _length:
            """
            count = 0
            current_len = 0
            power = 0
            damage_length = len(_conflict_damage)
            while _n < damage_length - 1:
                if _conflict_damage[_n][3] >= _conflict_damage[_n + 1][2]:
                    temp_damage_index_copy = copy.copy(_conflict_damage)
                    temp_damage_index_copy_2 = copy.copy(_conflict_damage)
                    del temp_damage_index_copy[_n]
                    del temp_damage_index_copy_2[_n + 1]
                    decisions(temp_damage_index_copy, 0, _length)
                    decisions(temp_damage_index_copy_2, 0, _length)
                    break
                else:
                    count += 1
                _n += 1
            if count == damage_length - 1:
                for i in range(damage_length):
                    power += constans.SPELLS[_conflict_damage[i][0]]
                    current_len += _conflict_damage[i][4]
                power -= (_length - current_len)
                damage_list.append(power)

        decisions(conflict_damage, n, length)
        return max(damage_list), length
    raise ValueError("Too short conflict list")
