# coding=utf-8
"""
MAIN FUNCTION
"""
from damage_module import core


def main():
    """
    main
    """
    example_spell_list = ['feeai',
                          'feaineai',
                          'jee',
                          'fefefefefeaiaiaiaiai',
                          'fdafafeajain',
                          'fexxxxxxxxxxai',
                          'feee',
                          'xxxxxfejejeeaindaiyaiaixxxxxx',
                          'jejefeai']

    for example_spell in example_spell_list:
        print("{0}: {1}".format(example_spell, core.damage(example_spell)))


if __name__ == "__main__":
    # execute only if run as a script
    main()
