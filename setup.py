# -*- coding: utf-8 -*-
"""
SETUP
"""

from setuptools import setup, find_packages


with open('README.rst') as f:
    readme = f.read()

with open('LICENSE') as f:
    lic = f.read()

setup(
    name='damage',
    version='0.1.0',
    description='Damage package',
    long_description=readme,
    author='Michal Grzelak',
    author_email='grzelak.michael@gmail.com',
    url='https://bitbucket.org/grzelakmichal/damage',
    license=lic,
    packages=find_packages(exclude=('tests', 'docs'))
)
