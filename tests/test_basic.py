# coding=utf-8
"""
BASIC TESTS
"""
import random

from unittest import TestCase
from unittest import main
from unittest.mock import patch

from tests.context import damage_module


class TestFirstOccurrenceFromEnd(TestCase):
    @classmethod
    def setUpClass(cls):
        super(TestFirstOccurrenceFromEnd, cls).setUpClass()
        cls.string = "abcdeabcde"  # concat two same string

    def test_last_element(self):
        substring = self.string[-1]
        self.assertEqual(damage_module.first_occurrence_from_end(self.string, substring), len(self.string) - 1)

    def test_first_element(self):
        substring = self.string[0]
        self.assertEqual(damage_module.first_occurrence_from_end(self.string, substring), int(len(self.string) / 2))

    def test_random_element(self):
        random_index = random.randint(0, len(self.string) - 1)
        substring = self.string[random_index]
        index = self.string.index(substring)
        self.assertEqual(damage_module.first_occurrence_from_end(self.string, substring),
                         index + int(len(self.string) / 2))


class TestIsOccurOnceAndOnlyOnce(TestCase):
    @classmethod
    def setUpClass(cls):
        super(TestIsOccurOnceAndOnlyOnce, cls).setUpClass()
        cls.string = "aabccdee"

    def test_substring_occur_once(self):
        substring = "b"
        self.assertTrue(damage_module.is_occur_once_and_only_once(self.string, substring))

    def test_substring_occur_more_than_once(self):
        substring = "c"
        self.assertFalse(damage_module.is_occur_once_and_only_once(self.string, substring))

    def test_substring_not_occur(self):
        substring = "f"
        self.assertFalse(damage_module.is_occur_once_and_only_once(self.string, substring))


class TestGenerateAllSubspells(TestCase):
    def test_case_1(self):
        spell = 'feeai'
        self.assertEqual(damage_module.generate_all_subspells(spell), [('fe', 1, 0, 1, 2), ('ai', 2, 3, 4, 2)])

    def test_case_2(self):
        spell = 'fejejeeaindaiyaiai'
        self.assertEqual(damage_module.generate_all_subspells(spell), [('fe', 1, 0, 1, 2), ('je', 2, 2, 3, 2),
                                                                       ('je', 2, 4, 5, 2), ('jee', 3, 4, 6, 3),
                                                                       ('ai', 2, 7, 8, 2), ('ain', 3, 7, 9, 3),
                                                                       ('dai', 5, 10, 12, 3), ('ai', 2, 11, 12, 2),
                                                                       ('ai', 2, 14, 15, 2), ('ai', 2, 16, 17, 2)])

    def test_case_empty_string(self):
        spell = ''
        self.assertEqual(damage_module.generate_all_subspells(spell), [])

    def test_string_without_subspells(self):
        spell = 'abrakadabra'
        self.assertEqual(damage_module.generate_all_subspells(spell), [])


class TestMaxDamage(TestCase):
    def test_case_1(self):
        conflict_subspell_list = [('ai', 2, 5, 6, 2), ('ain', 3, 5, 7, 3)]
        self.assertEqual(damage_module.max_damage(conflict_subspell_list), (3, 3))

    def test_case_2(self):
        conflict_subspell_list = [('ai', 2, 0, 1, 2), ('ain', 3, 0, 2, 3), ('ne', 2, 2, 3, 2)]
        self.assertEqual(damage_module.max_damage(conflict_subspell_list), (4, 4))

    def test_empty_list(self):
        conflict_subspell_list = []
        with self.assertRaises(ValueError) as context:
            damage_module.max_damage(conflict_subspell_list)
        self.assertTrue("Too short conflict list" in str(context.exception))


class TestDamage(TestCase):
    def test_case_1(self):
        spell = 'feeai'
        self.assertEqual(damage_module.damage(spell), 2)

    @patch('damage_module.constans.SPELLS', {'fe': 2, 'je': 2, 'jee': 3, 'ain': 3, 'dai': 5, 'ne': 2, 'ai': 2})
    def test_case_2(self):
        spell = 'feeai'
        self.assertEqual(damage_module.damage(spell), 3)

    @patch('damage_module.constans.SPELLS', {'fe': 2, 'je': 2, 'jee': 3, 'ain': 3, 'dai': 5, 'ne': 2, 'ai': 10})
    def test_case_3(self):
        spell = 'feeai'
        self.assertEqual(damage_module.damage(spell), 11)

    @patch('damage_module.constans.ENDING_SUBSPELL', 'e')
    @patch('damage_module.constans.SPELLS', {'fe': 1, 'e': 2, 'jee': 3, 'ain': 3, 'dai': 5, 'ne': 2, 'ai': 2})
    def test_case_4(self):
        spell = 'feeai'
        self.assertEqual(damage_module.damage(spell), 3)

    def test_case_5(self):
        spell = 'feaineai'
        self.assertEqual(damage_module.damage(spell), 7)

    @patch('damage_module.constans.SPELLS', {'fe': 1, 'je': 2, 'jee': 3, 'ain': 3, 'dai': 5, 'ne': 0, 'ai': 2})
    def test_case_6(self):
        spell = 'feaineai'
        self.assertEqual(damage_module.damage(spell), 5)

    @patch('damage_module.constans.SPELLS', {'fe': 1, 'je': 2, 'jee': 3, 'ain': 3, 'dai': 5, 'ne': 2, 'ai': 2, 'in': 7})
    def test_case_6(self):
        spell = 'feaineai'
        self.assertEqual(damage_module.damage(spell), 8)

    @patch('damage_module.constans.BEGINNING_SUBSPELL', 'e')
    @patch('damage_module.constans.SPELLS', {'fe': 1, 'e': 2, 'jee': 3, 'ain': 3, 'dai': 5, 'ne': 2, 'ai': 2})
    def test_case_4(self):
        spell = 'feai'
        self.assertEqual(damage_module.damage(spell), 4)

if __name__ == '__main__':
    main()
